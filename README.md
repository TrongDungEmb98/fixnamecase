FixNameCase
===========


Tool to change case of variable names, function names in C++ source code.

Usage
-----

```
fix-name-case input_folder
```

Requirements
------------

[Universal Ctags](http://ctags.io/) is installed.
